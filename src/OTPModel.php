<?php

namespace Servetel\OTPSignIn;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OTPModel extends Model
{
    protected $guarded = [];
    protected $table = 'otp';
    public $timestamps = false;
    protected $fillable = ['mobile', 'email','email_otp','sms_otp', 'sms_otp_creation_time','email_otp_creation_time','email_otp_expiration_time', 'sms_otp_expiration_time','email_otp_verified','sms_otp_verified', 'sms_otp_sent_time','email_otp_sent_time','url','response'];


    public static function create($array = array()){
        try {
            $otp = new OTPModel;
            $otp->mobile = @$array['number'];
            $otp->email = @$array['email'];
            $otp->email_otp = @$array['email_otp'];
            $otp->sms_otp = @$array['sms_otp'];
            $otp->sms_otp_creation_time = @$array['sms_otp_creation_time'];
            $otp->email_otp_creation_time = @$array['email_otp_creation_time'];
            $otp->email_otp_expiration_time = @$array['email_otp_expiration_time'];
            $otp->sms_otp_expiration_time = @$array['sms_otp_expiration_time'];
            $otp->email_otp_verified = @$array['email_otp_verified'];
            $otp->sms_otp_verified = @$array['sms_otp_verified'];
            $otp->sms_otp_sent_time = @$array['sms_otp_sent_time'];
            $otp->email_otp_sent_time = @$array['email_otp_sent_time'];
            $otp->user_id = @$array['user_id'];
            $otp->url = @$array['url'];
            $otp->response = @$array['response'];
            $otp->save();
            return $otp->id;
        } catch (\Exception $e){
            return array(
                'error' => true,
                'message' => $e->getMessage(),
            );

        }
    }

    public static function get_details($array = array()){
        try {
            $otp = DB::table('otp')->where([['id','=',$array['sms_id']],['sms_otp','=',$array['otp_phone']],['sms_otp_verified','=',0],['sms_otp_expiration_time','>=',date('Y-m-d H:i:s')]])->get()->toArray();
           if($otp && !empty($otp)){
               $update = DB::table('otp')->where([['id','=',$array['sms_id']],['sms_otp','=',$array['otp_phone']],['sms_otp_verified','=',0]])->update(['sms_otp_verified' => 1]);
               return $otp[0]->user_id; //OTP Verified
           } else {
               return "-10"; //OTP Invalid
           }
        } catch (\Exception $e){
            return array(
                'error' => true,
                'message' => $e->getMessage(),
            );

        }
    }
}