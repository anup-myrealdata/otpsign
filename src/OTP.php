<?php

namespace Servetel\Otpsignin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OTP extends Model
{
    //
    public static function send_otp($number,$user_id){

        try {
            $key = mt_rand(1000, 9999);
            $body = "$key is your Servetel Verification Code.";
            $defaultGateway = DB::table('sms_gateway')->select('*')->where([['default_gateway', '=', 1], ['deleted_at', '=', NULL],['admin_id','=',1]])->get();

            $gateway["username"] = $defaultGateway[0]->gateway_username;
            $gateway["password"] = $defaultGateway[0]->gateway_password;
            $gateway["number"] = $number;
            $gateway["senderid"] = $defaultGateway[0]->default_sender_id;
            $gateway["msg"] = rawurlencode($body);
            $gateway["type"] = "";

            if($defaultGateway[0]->gateway_name == 'gbl_sms'){
                $gateway_url = $defaultGateway[0]->gateway_url . "&gwid=2";
            } elseif($defaultGateway[0]->gateway_name == 'value_first'){
                $gateway_url = $defaultGateway[0]->gateway_url;
            }

            $url = $gateway_url;
            foreach($gateway as $k=>$v)
            {
              $url = str_replace("{".$k."}", $v, $url);
            }

            $response = file_get_contents($url);

            $current_date = date('Y-m-d H:i:s');
            $dataArray = array(
                'number' => $number,
                'sms_otp' => $key,
                'sms_otp_creation_time' => $current_date,
                'sms_otp_sent_time' => $current_date,
                'sms_otp_expiration_time' => date("Y-m-d H:i:s", strtotime("+15 minutes")),
                'sms_otp_verified' => 0,
                'user_id' => $user_id,
                'url' => $url,
                'response' => $response
            );
            $id = OTPModel::create($dataArray);

            if(!is_array($id) && !empty($id)){
                return $id;
            } else {
                return '-10';
            }
        } catch (\Exception $e){
            return array(
                'error' => true,
                'message' => $e->getMessage(),
            );
        }
    }

    public function verify_otp($sms_id,$otp_phone){
        try{
            $dataArray = array(
                'otp_phone' => $otp_phone,
                'sms_id' => $sms_id,
            );
            $response = OTPModel::get_details($dataArray);
            if(!is_array($response) && !empty($response)){
                return $response;
            } else {
                return '-20';
            }
        } catch (\Exception $e){
            return array(
                'error' => true,
                'message' => $e->getMessage(),
            );
        }
    }
}
